namespace Oglasnik.Models {
  public class Cars {
    public static List<Car> GetAll() {
      List<Car> cars = new List<Car>();
      cars.Add(new Car("Mazda", "3"));
      cars.Add(new Car("Volkswagen", "Golf"));
      return cars;
    }
  }
}