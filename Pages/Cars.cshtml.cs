﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Oglasnik.Models;

namespace Oglasnik.Pages;

public class CarsModel : PageModel {
  private readonly ILogger<CarsModel> _logger;

  public List<Car> AllCars { get; set; }

  public CarsModel(ILogger<CarsModel> logger) {
    _logger = logger;
    AllCars = Cars.GetAll();
  }

  public void OnGet() {
  }
}
